import 'package:flutter/material.dart';
import 'package:whatsapp/model/status_model.dart';

class Status extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        new Row(
          children: [
            Expanded(
                child: ListTile(
              leading: Container(
                width: 50,
                child: Stack(
                  children: [
                    ClipOval(
                      child: Image.network(
                          "https://randomuser.me/api/portraits/men/10.jpg",
                          height: 50,
                          width: 50),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.circular(20.0)),
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              title: Text("My Status",
                  style: TextStyle(fontWeight: FontWeight.bold)),
              subtitle: Text("Tap to add status update"),
            )),
          ],
        ),
        SizedBox(
          height: 30,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              width: double.infinity,
              color: Colors.grey.shade200,
              child: Text('Recent Updates',
                  style: TextStyle(
                      color: Colors.grey.shade500,
                      fontWeight: FontWeight.bold))),
        ),
        Expanded(
            child: ListView.builder(
                itemCount: statusData.length,
                itemBuilder: (context, i) => new Column(
                      children: [
                        new Divider(height: 0.1),
                        new ListTile(
                          leading: CircleAvatar(
                            backgroundColor: Colors.grey,
                            backgroundImage: NetworkImage(statusData[i].pic),
                          ),
                          title: Text(statusData[i].name),
                          subtitle: Text(
                            statusData[i].time,
                            style: TextStyle(color: Colors.grey, fontSize: 13),
                          ),
                        )
                      ],
                    )))
      ],
    );
  }
}
