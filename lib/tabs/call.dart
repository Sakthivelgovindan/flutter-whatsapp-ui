import 'package:flutter/material.dart';
import 'package:whatsapp/model/call_model.dart';

class Call extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: callsData.length,
        itemBuilder: (context, i) => Container(
              padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
              child: new Column(
                children: [
                  Container(
                    color: Colors.grey,
                    height: 0.1,
                  ),
                  new ListTile(
                    leading: CircleAvatar(
                        backgroundColor: Colors.grey,
                        backgroundImage: NetworkImage(callsData[i].pic)),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Text(callsData[i].name),
                        new Icon(i % 2 == 0 ? Icons.call : Icons.video_call,
                            color: Theme.of(context).primaryColor)
                      ],
                    ),
                    subtitle: new Text(callsData[i].time),
                  ),
                ],
              ),
            ));
  }
}
