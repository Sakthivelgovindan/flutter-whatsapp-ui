import 'package:flutter/material.dart';
import 'package:whatsapp/model/chat_model.dart';

class Chat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: chatsData.length,
        itemBuilder: (context, i) => Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: new Column(
                children: [
                  Container(
                    color: Colors.grey,
                    height: 0.1,
                  ),
                  new ListTile(
                    leading: CircleAvatar(
                        backgroundColor: Colors.grey,
                        backgroundImage: NetworkImage(chatsData[i].pic)),
                    title: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Text(chatsData[i].name,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold)),
                          new Text(chatsData[i].time,
                              style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey))
                        ]),
                    subtitle: new Text(chatsData[i].msg,
                        style: TextStyle(color: Colors.grey)),
                  ),
                ],
              ),
            ));
  }
}
