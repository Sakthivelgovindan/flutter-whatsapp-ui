import 'package:flutter/material.dart';
import 'package:whatsapp/tabs/call.dart';
import 'package:whatsapp/tabs/camera.dart';
import 'package:whatsapp/tabs/chat.dart';
import 'package:whatsapp/tabs/status.dart';

void main() => runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          primaryColor: new Color(0xff075e54),
          accentColor: new Color(0xff25d366)),
      home: MyApp(),
    ));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();

    _tabController = new TabController(length: 4, vsync: this, initialIndex: 1)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Whatsapp'),
        brightness: Brightness.dark,
        actions: [
          IconButton(onPressed: () {}, icon: Icon(Icons.search)),
          IconButton(onPressed: () {}, icon: Icon(Icons.more_vert))
        ],
        bottom: TabBar(
          indicatorColor: Colors.white,
          controller: _tabController,
          tabs: [
            new Container(
                width: 10.0, child: new Tab(icon: Icon(Icons.camera_alt))),
            Tab(text: 'CHATS'),
            Tab(text: 'STATUS'),
            Tab(text: 'CALLS')
          ],
        ),
      ),
      body: new TabBarView(
          controller: _tabController,
          children: [new Camera(), new Chat(), new Status(), new Call()]),
      floatingActionButton: _tabController.index == 0
          ? FloatingActionButton(
              child: Icon(Icons.camera_alt, color: Colors.white),
              onPressed: () {})
          : _tabController.index == 1
              ? FloatingActionButton(
                  child: Icon(Icons.message, color: Colors.white),
                  onPressed: () {})
              : _tabController.index == 2
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(
                          height: 45,
                          width: 45,
                          child: FloatingActionButton(
                              backgroundColor: Colors.white,
                              child: Icon(Icons.edit, color: Colors.blueGrey),
                              onPressed: () {}),
                        ),
                        SizedBox(height: 10.0),
                        FloatingActionButton(
                            child: Icon(Icons.camera_alt, color: Colors.white),
                            onPressed: () {})
                      ],
                    )
                  : FloatingActionButton(
                      child: Icon(Icons.add_call, color: Colors.white),
                      onPressed: () {}),
    );
  }
}
