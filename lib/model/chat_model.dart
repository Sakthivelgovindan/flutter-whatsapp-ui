class ChatModel {
  final String name;
  final String msg;
  final String time;
  final String pic;

  ChatModel(this.name, this.msg, this.time, this.pic);
}

List<ChatModel> chatsData = [
  new ChatModel(
      "Android",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "9.30pm",
      "https://randomuser.me/api/portraits/men/2.jpg"),
  new ChatModel(
      "Moto G3",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "9.00pm",
      "https://randomuser.me/api/portraits/men/3.jpg"),
  new ChatModel(
      "Apple",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "8.00pm",
      "https://randomuser.me/api/portraits/men/4.jpg"),
  new ChatModel(
      "MacOS",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "8.30pm",
      "https://randomuser.me/api/portraits/men/5.jpg"),
  new ChatModel(
      "Blackberry",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "7.00pm",
      "https://randomuser.me/api/portraits/men/6.jpg"),
  new ChatModel(
      "Windows",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "7.30pm",
      "https://randomuser.me/api/portraits/men/7.jpg"),
  new ChatModel(
      "John and Joe",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "10.30pm",
      "https://randomuser.me/api/portraits/men/8.jpg"),
  new ChatModel(
      "Work Space",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "6.30pm",
      "https://randomuser.me/api/portraits/men/9.jpg"),
  new ChatModel(
      "Random account",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "5.30pm",
      "https://randomuser.me/api/portraits/men/10.jpg"),
  new ChatModel(
      "Tom",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "10.30pm",
      "https://randomuser.me/api/portraits/men/1.jpg"),
  new ChatModel(
      "Jerry",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "10.00pm",
      "https://randomuser.me/api/portraits/men/2.jpg"),
];
