class StatusModel {
  final String name;
  final String time;
  final String pic;

  StatusModel(this.name, this.time, this.pic);
}

List<StatusModel> statusData = [
  new StatusModel(
      "Android", "9.30pm", "https://randomuser.me/api/portraits/men/2.jpg"),
  new StatusModel(
      "Moto G3", "9.00pm", "https://randomuser.me/api/portraits/men/3.jpg"),
  new StatusModel(
      "Apple", "8.00pm", "https://randomuser.me/api/portraits/men/4.jpg"),
  new StatusModel(
      "MacOS", "8.30pm", "https://randomuser.me/api/portraits/men/5.jpg"),
  new StatusModel(
      "Blackberry", "7.00pm", "https://randomuser.me/api/portraits/men/6.jpg"),
  new StatusModel(
      "Windows", "7.30pm", "https://randomuser.me/api/portraits/men/7.jpg"),
  new StatusModel("John and Joe", "10.30pm",
      "https://randomuser.me/api/portraits/men/8.jpg"),
  new StatusModel(
      "Work Space", "6.30pm", "https://randomuser.me/api/portraits/men/9.jpg"),
  new StatusModel("Random account", "5.30pm",
      "https://randomuser.me/api/portraits/men/10.jpg"),
  new StatusModel(
      "Tom", "10.30pm", "https://randomuser.me/api/portraits/men/1.jpg"),
  new StatusModel(
      "Jerry", "10.00pm", "https://randomuser.me/api/portraits/men/2.jpg"),
];
