class CallModel {
  final String name;
  final String time;
  final String pic;

  CallModel(this.name, this.time, this.pic);
}

List<CallModel> callsData = [
  new CallModel(
      'Android', '12:30 PM', "https://randomuser.me/api/portraits/men/12.jpg"),
  new CallModel('Blackberry', '2:30 PM',
      "https://randomuser.me/api/portraits/men/13.jpg"),
  new CallModel(
      'CuberNets', '9:30 PM', "https://randomuser.me/api/portraits/men/14.jpg"),
  new CallModel(
      'Android', '12:30 PM', "https://randomuser.me/api/portraits/men/15.jpg"),
  new CallModel('Blackberry', '2:30 PM',
      "https://randomuser.me/api/portraits/men/16.jpg"),
  new CallModel(
      'CuberNets', '9:30 PM', "https://randomuser.me/api/portraits/men/17.jpg"),
  new CallModel(
      'Android', '12:30 PM', "https://randomuser.me/api/portraits/men/18.jpg"),
  new CallModel('Blackberry', '2:30 PM',
      "https://randomuser.me/api/portraits/men/19.jpg"),
  new CallModel(
      'CuberNets', '9:30 PM', "https://randomuser.me/api/portraits/men/14.jpg"),
];
